//
//  CategoriesTableViewControllerDelegate.swift
//  avp_ios
//
//  Created by kayeli dennis on 11/12/2017.
//  Copyright © 2017 kayeli dennis. All rights reserved.
//

import Foundation

extension CategoriesTableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        // update the selected Candidate
        toggleCellCheckbox(cell, at: indexPath.row)
    }

    func toggleCellCheckbox(_ cell: UITableViewCell, at: Int) {
        cell.accessoryType = cell.accessoryType == .none ? .checkmark : .none
        // update the selectedCandidate
        if cell.accessoryType == .none {
            viewModel?.deleteCandidateAt(at: at)
        }else {
            viewModel?.candidateSelectedAt(at: at)
        }
    }
}
