//
//  CategoriesViewModel.swift
//  avp_ios
//
//  Created by kayeli dennis on 10/12/2017.
//  Copyright © 2017 kayeli dennis. All rights reserved.
//

import Foundation
import RxSwift

protocol AwardDelegate {
    func currentAwardChanged(with award: Award?, value: Bool)
}

class CategoriesViewModel{

    let awards: Variable<[Award]> = Variable([])

    let disposeBag = DisposeBag()

    var currentAward: Award?

    let selectedCandidate: Variable<[Award.Nominee]> = Variable([])

    var awardDelegate: AwardDelegate?

    func numberOfRows()-> Int{
        guard let current = currentAward else { return 0 }
        return current.nominees.count
    }

    init() {

        // Kick Off awards fetch
        self.fetchAwards()

        // Listen for Awards change
//            self.awards
//                .asObservable()
//                .subscribe { (awards) in
//                    // clear the current award first
//                    self.currentAward = nil
//
//                    awards.element?.forEach({ (award) in
//                        print("the award: ", award)
//                        if award.votingOpen {
//                            self.currentAward = award
//                        }
//                    })
//                    self.awardDelegate?.currentAwardChanged(with: self.currentAward, value: true)
//        }.disposed(by: disposeBag)
    }

    func numberOfSections()-> Int{
        return 1
    }

    func fetchAwards() {
        NetworkController.fetchAwards(){ awards in
            self.currentAward = nil

            awards.forEach({ (award) in
                print("the award: ", award)
                if award.votingOpen {
                    self.currentAward = award
                }
            })
            self.awardDelegate?.currentAwardChanged(with: self.currentAward, value: true)
        }
    }

    func candidateSelectedAt(at position: Int){
        // Be certain we have an open Award
        guard let currentAward = currentAward else { return }
        selectedCandidate.value.append(currentAward.nominees[position])
    }

    func deleteCandidateAt(at position: Int){
        if !self.selectedCandidate.value.isEmpty {
            if self.selectedCandidate.value.indices.contains(position){
                self.selectedCandidate.value.remove(at: position)
            }
        }
    }
}
